
# My own streaming platform
This is a quick guide that will help you set up your own Netflix like streaming server. 

How it works? 

Let's setup an app that is easy to use to download torrents (these will be [Sonarr](https://sonarr.tv/) and [Radarr](https://radarr.video/)), connect them to an indexer (this will be [Prowlarr](https://github.com/Prowlarr/Prowlarr) and [Jackett](https://github.com/Jackett/Jackett) and you can think of this as a search engine) and we will also need a torrent client ([qBittorrent](https://www.qbittorrent.org/)) that will download the torrents to our machine. Now the only thing that remains is to stream them somehow via a clean UI (and for this I've chosen ~~[Plex](https://www.plex.tv/)~~ [JellyFin](https://jellyfin.org/)).
After switching to JellyFin, I was not really getting together with the OpenSubtitle plugin, so at last I've also added the [Bazarr](https://www.bazarr.media/)

## What you will need
- some basic knowledge about servers, networking, docker
 - a server that is accessible from the outside of you network (for ex. static IP/DDNS/your own domain)
- this server must have docker and docker-compose installed

### Server config
- I already had a spare machine in my home used for game servers, so I've chosen it as host machine for this whole setup. It has an  `Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-139-generic x86_64)` installed and is using the Uncomplicated firewall (`ufw`) . This machine is currently accessible from the outside world via a domain that is supplied by my Internet Service Provider (ISP). More about port and firewall configs in the [#configs] section.
- I have a `steam` user: 

>     steam@lemontree:~$ id
>     uid=1001(steam) gid=1001(steam) groups=1001(steam),998(docker)
- the docker-compose.yml is configured based on the above information - `PUID` and `GUID` is set to `1001`. This way, the files created on the machine, through the attached volumes, will have the `steam` user as owner.
- docker install and config for linux can be found [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository). Don't forget the [postinstall](https://docs.docker.com/engine/install/linux-postinstall/) steps - my `steam` user does not have any `sudo` rights.

### Sonarr

> Sonarr is a PVR for Usenet and BitTorrent users. It can monitor
> multiple RSS feeds for new episodes of your favorite shows and will
> grab, sort and rename them.

Okay, [here's](https://www.reddit.com/r/sonarr/comments/4gw7yo/what_is_sonarr/) an easier description of Sonarr: 

> Sonarr automates your TV downloads from public and private trackers.
> That's pretty much it.

It comes with an easy to understand and use UI. It just needs some configurations.

**IMPORTANT:** Sonarr is specialized for series. It won't really find any standalone movies. That's why we will also use Radarr.

### Radarr
Same as Sonarr, but specialized for movies.

**IMPORTANT:** No, you won't be able to use either Sonarr or Radarr to download [adult content](https://www.reddit.com/r/radarr/comments/si6gtm/adult_content/)!

### qBittorrent

> The qBittorrent project aims to provide an open-source software
> alternative to µTorrent.

### Prowlarr

> Prowlarr is an indexer manager/proxy ... . It integrates seamlessly with 
> ... Radarr, Readarr, and Sonarr offering complete management of your 
> indexers with no per app Indexer setup required (we do it all).

At first I was using Jackett but Prowlarr has a much better integration. However the ncore tracker which I use for hungarian torrent was not working with Prowlarr so I have kept Jackett as well and only for that tracker.

Can be set up easily, connect them to sonarr and radarr (using these as hostnames since we are in a docker environment).

### Jackett

> Jackett works as a proxy server: it translates queries from apps
> (Sonarr, Radarr, etc.) into tracker-site-specific http queries, parses
> the html or json response, and then sends results back to the
> requesting software.

In short: you search for a movie on the Radarr UI, the Radarr send that search to the Jackett - please find this torrent for me - the Jackett looks for it and sends the found information back to Radarr.

### Bazarr

> Bazarr is a companion application to Sonarr and Radarr that 
> manages and downloads subtitles based on your requirements.

Can be set up easily, connect them to sonarr and radarr (using these as hostnames since we are in a docker environment). Create a language profile and download the desired subtitles. As a provider I've added the opensubtitles.com

### Plex

> Plex gives you one place to find and access all the media that matters
> to you. From personal media on your own server, to free and on-demand
> Movies & Shows or live TV, to streaming music, you can enjoy it all in
> one app, on any device.

### JellyFin

> Jellyfin is the volunteer-built media solution that puts you in control 
> of your media. Stream to any device from your own server, with no strings 
> attached. Your media, your server, your way.

In short: same as Plex, but the features that are paid in Plex, here are free. It also supports plugins, so you can customize it more freely.

## Configs
### Explanation of docker-compose
- I've set everything up inside the home folder of the `steam` user, inside a `sonarr` folder. This means that everything is inside `/home/steam/sonarr`
- You should change these paths to your own needs
- qBittorrent downloads everything into the `/downloads` folder, so I've attached my `/home/steam/sonarr/downloads` folder as a volume to it
- the same `/home/steam/sonarr/downloads` folder will be used by Sonarr and Radarr for downloads (Sonarr and Radarr does some renaming and creates [hard links](https://en.wikipedia.org/wiki/Hard_link) to the downloaded files).
- all of the apps need a `config` folder, so they have been also attached as volumes
- the JellyFin inside the container uses `8096` port, and I was lazy to change any router and firewall configs, so outside it will be *redirected* to `32400` (same as Plex was running on).


### Port configs for server and router
- Plex uses the port `32400` by default, so you I had to allow that port via the firewall to be able to access it from my home network:
>     ufw allow 32400
- We will also need to allow the Sonarr (port 8989) and Radarr (port 7878), if we want to access it:
>     ufw allow 7878
>     ufw allow 8989
- We will need to grab some configs from Jackett that will be used by Sonarr and Radarr, so we allow the port used by Jackett (port 9117) as well
>     ufw allow 9117
- Out of these apps, the only one I want to access from outside my home network is the Plex, so I've configured my router to forward anything from port 32400 to the machine where this whole setup is running, to the port 32400.
- I've also allowed the ports 9118 (Prowlarr see below) and 6767 (Bazarr see below)

### Sonarr and Radarr config
- Assume from now on that my server is accessbile via a static IP in my home network and that is `192.168.1.157`
- Radarr page: `192.168.1.157:7878`
- Sonarr page: `192.168.1.157:8989`
- Jackett page: `192.168.1.157:9117`
- Prowlarr page: `192.168.1.157:9118` - I've remapped from 9696 to 9118 outside, but when doing the configuration you still have to use the 9696 (docker world)
- Bazarr page: `192.168.1.157:6767`
- First of all, you will need to add the Jackett indexers (or Prowlarr indexers or maybe both)
    - I've only added *rarbg* and *1337x*
    - Please see [this](https://community.seedboxes.cc/articles/how-to-use-jackett-with-sonarr) for how to do it (start from the jackett subtitle)
- Add qBittorrent as torrent client to Sonarr and Radarr
    - Open up both of these apps
    - Settings -> Download Clients -> + icon -> Torrents -> qBittorrent
        - Name: qBittorrent
        - Host: qBittorrent (same as service name in docker-compose)
        - Port: 8080 (as long as you did not change it in docker-compose)
        - Username: admin (it's the default value)
        - Password: adminadmin (it's the default value)
- Change root folder
    - Settings -> Media Management -> Root folders (at the bottom)
    - Add root folder
        - `/tv` for Sonarr
        - `/movies` for Radarr
    - This way, all downloads from qBittorent go to the `/downloads` folder, but when they are done, a hard link will be created to them in the `/tv` and `/movies` folder, depending on what you've downloaded
    - Remove the `/downloads` from Root folder

### Plex config
- Plex is accessible via `192.168.1.157:32400`
- You will need to create and account to sign in (I've used my google account)
- Here you will have to make some configurations
- To access the settings: an icon the right top of the page -> Account Settings -> Left part of the screen ->
    - Settings -> Remote access -> allow it
        - Also, Show Advanced -> Manually specify public port -> 32400
        - This will allow Plex to connect to this server and you can watch your media from anywhere via the Plex App
    - Manage -> Libraries
        - Movies -> connect it to `/movies`
        - TV Shows -> connect it to `/tv`

### JellyFin config
- mostly the same configs apply as for Plex
- accessible via `192.168.1.157:32400`
- create default (admin) user
- configre some libraries (TVShows and Movies)
- add users if needed
- add plugins if needed


# Afterword
Now you should be able download movies/series via Radarr and Sonarr, and stream them anywhere via [Plex](https://app.plex.tv/desktop/#!/) (authentication still required).

- After the configs, I've removed the `9117` port from `ufw`, since I do not want to access Jackett anymore at all
- I did not set a password for Radarr or Sonarr because they are only accessible from inside my network
- I did **not** `port forward 7878` and `8989` since I do not want to access Sonarr or Radarr from outside my home network
- In the GitLab repo you will also find a `home_cinema.service` file which I'm using to startup all these services via a `systemctl --user start home_cinema` call. I've placed that file into `~/.config/systemd/user` folder.

# Is it legal?
- Well, of coure not! You are downloading (**pirating!**)  movies.
- Have fun

# Some images
## Sonarr
![Sonarr](img/sonarr.png)
## Radarr
![Radarr](img/radarr.png)
## Plex
![Plex](img/plex.png)
## JellyFin
![JellyFin](img/jellyfin.png)